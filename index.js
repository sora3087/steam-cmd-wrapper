const EventEmitter = require('events');
// const { spawn } = require('child_process');
var os = require('os');
var pty = require('node-pty');
var shell = os.platform() === 'win32' ? 'powershell.exe' : 'bash';

const Path = require('path');
const fs = require('fs');

class SteamChild extends EventEmitter{
  constructor(path, parent){
    super();

    this.parent = parent;
    this.path = path; //this is the path of the binary for SteamCMD
    this.busy = false; //SteamChild is currently running a command
    this.loggedIn = false; //Reports the current logged in state of the SteamChild
    this.outputBuffer = ""; //The current output buffer of the underlying terminal

    //Exposes the underlying terminal for the SteamChild
    this.cmd = pty.spawn(shell, [], {
      name: 'cmd',
      cols: 100,
      rows: 30,
      cwd: process.env.HOME,
      env: process.env
    });

    return this;
  }
  boot(){
    this.busy = true;
    this.cmd.on('data', (data) => {
      var lines = data.split(/[\r\n]{1,2}/g);
      lines.forEach((line) => {
        var progress = line.match(/\[([^\[\]]+)\]\s([^\.]+)\.{1,3}/);
        if (progress) {
          var message = {text: "", value: 0};
          if (progress[1] != "----") {
            message.value = parseInt(progress[1])/100;
          }
          message.text = progress[2];
          this.parent.emit("progress", message);
        }else{
          if (line.indexOf("Steam>") > -1) {
            this.cmd.removeAllListeners('data');
            this.cmd.on('data', ( chunk ) => this.stdout(this, chunk) );
            this.busy = false;
            this.emit('ready');
          }
        }
      });
    });
    this.cmd.write(this.path+"\r");
  }
  stdout(parent, chunk){
    //This function attempts to respond with the return after a command is run
    parent.outputBuffer += chunk;
    if (parent.outputBuffer.indexOf("Steam>") > -1 || parent.outputBuffer.indexOf("Two-factor code:") > -1 || parent.outputBuffer.indexOf("password:") > -1) {
      parent.outputBuffer.replace('\nSteam>', '');
      parent.emit('return', parent.outputBuffer.replace(/\u001b/g, '').replace(/\[((\d{1,3})G|0(K|(;1)?m)|\?25(h|l))/g, ''));
    }
  }
  command(name, params){
    console.log('command '+name);
    //This function attempts to only collapse a Promise once a function has returned completely
    this.busy = true;
    return new Promise((resolve, reject) => {
      var command = name;
      if (params) {
        command += ' '+params.join(' ');
      }
      this.once('return', (response) => {
        console.log('command returned');
        this.busy = false;
        resolve(response);
      });
      this.outputBuffer = "";
      this.cmd.write(command+"\r");
    });
  }
  queue(command, params){
    console.log('queue '+name);
    return new Promise((resolve, reject) => {
      this.once('return', () => {
        this.command(command, params)
          .then((response) => {
            resolve(response);
          });
      })
    });
  }
}

module.exports = class extends EventEmitter{
  constructor(path){
    super();
    this.binPath = Path.normalize(path);
    this.appPath = this.binPath+"steamcmd.exe";
    this.outputBuffer = "";
    return this;
  }
  init(){
    try {
      fs.statSync(this.appPath);
      this.start();
    }catch(e){
      this.downloadClient();
    }
  }
  downloadClient(){
    var https = require('https');
    var ZIP = require("zip");
    var url = "https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip";
    https.get(url, (res) => {
      var length = res.headers['content-length'];
      var downloadBuffer = Buffer.alloc(parseInt(length));
      var pointer = 0
      res.on('data', (data) => {
        data.copy(downloadBuffer, pointer);
        pointer += data.length;
        this.emit('progress', {text: "Downloading binary", value: pointer/length});
      }).on('end', () => {
        this.emit('unpacking');
        var reader = ZIP.Reader(downloadBuffer);
        reader.forEach((entry) => {
          var filename = entry.getName();
          if (filename.toLowerCase() == "steamcmd.exe") {
            var exe = entry.getData();
            fs.writeFile(this.appPath, exe, {flag: "w+"}, ()=>{
              this.start();
            });
          }
        });
      });
    });
  }
  start(){
    this.emit('starting');

    this.primary = new SteamChild(this.appPath, this);

    this.primary.cmd.on('end', function(){ console.log("primary ended"); });
    this.primary.cmd.on('close', function(){ console.log("primary closed"); });
    this.primary.cmd.on('finish', function(){ console.log("primary finished"); });
    this.primary.cmd.on('timeout', function(){ console.log("primary timed out"); });
    this.primary.cmd.on('drain', function(){ console.log("primary drained"); });
    this.primary.cmd.on('error', function(){ console.log("primary errored"); });
    this.primary.cmd.on('_socketEnd', function(){ console.log("primary socket ended"); });
    this.primary.cmd.on('ready_datapipe', function(){ console.log("primary datapipe ready"); });

    this.secondary = new SteamChild(this.appPath, this);
    this.primary.once('ready', () => {
      console.log('primary ready');
      this.primary.removeListener('progress', send_progress);
      this.secondary.boot();
    });
    this.secondary.once('ready', () => {
      console.log('secondary ready');
      this.emit('ready');
    });

    function send_progress(data){
      this.emit('progress', data);
    }
    this.primary.on('progress', send_progress);
    this.primary.boot();
    
    process.on('SIGINT', () => {
      this.primary.cmd.once('closed', () => {
        this.secondary.cmd.write("quit\r");
      });
      this.secondary.cmd.once('closed', () => {
        process.exit();
      });
      this.primary.cmd.write("quit\r");
    });
  }
  command(command, usePrimary, params){
    console.log('command router');
    //this is a router to send the command to an available child process
    if (usePrimary) {
      var fn = 'command';
      if (this.primary.busy) {
        //command needs and primary is busy
        //so we wait until the primary is not busy to run command
        fn = 'queue';
      }
      return this.primary[fn](command, params);
    }else{
      var child = null;
      if (this.secondary.busy) {
        child = new SteamChild(this.binPath);
      }else{
        child = this.secondary;
      }
      return child.command(command, params);
    }
  }
  login(username, password){
    //This runs on primary
    this.username = username;
    return this.command('login', true, [username, password]).then((response) => {
      if (response.indexOf("Steam>") > -1) {
        return true;
      }else{
        if (response.indexOf("Two-factor code:") > -1) {
          return 2;
        }else{
          return 1;
        }
      }
    });
  }
  enterCode(code){
    return this.command(code, true).then(response => {
      if (response.indexOf("Steam>") > -1) {
        return true;
      }else{
        return false;
      }
    })
  }
  logon(){
    return this.login();
  }
  logout(){
    return new Promise((resolve, reject) => {
      this.command('logout').then(response => {
        resolve();
      });
    });
  }
  logoff(){
    return this.logout();
  }
  install(id, directory){
    //Needs primary
  }
  uninstall(id){

  }
  getInfo(){
    console.log("getInfo");
    return this.command("info").then((info) => {
      var lines = info.split(/[\r\n]{1,2}/);
      var out = {};
      lines.forEach(line => {
        if (line != "") {
          var matches = line.match(/([^:]+):\s([^\r\n]+)/);
          if (matches) {
            if (matches[1] == "SteamID") {
              matches[2] = parseInt(matches[2].replace(/\[\]/g).split(":")[2]);
            }
            out[matches[1]] = matches[2];
          }
        }
      });
      return out;
    }).catch(function(){
      console.log('catch');
    });
  }
  getAppStatus(id){
    return new Promise((resolve, reject) => {
      this.command('app_status', false, id).then(response => {

        resolve();
      });
    });
  }
  getInstalled(){
    return new Promise((resolve, reject) => {
      this.command('apps_installed', false, id).then(response => {
        
        resolve();
      });
    });
  }
  getFriends(){
    //Needs primary
  }
}